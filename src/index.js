import React from 'react';
import ReactDOM from 'react-dom';
//import css here so all file can use it and no need import 1 by 1 
import './index.css';
import App from './components/App';
//
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
