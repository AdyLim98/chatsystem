import React,{useContext,useEffect,useState} from 'react';
import {useHistory} from 'react-router-dom';
import {auth} from '../firebase';

const AuthContext = React.createContext();
//create context and pass context data to it child
export const useAuth = () => useContext(AuthContext);

//2)
export const AuthProvider = ({children}) =>{
    const [loading,setLoading] = useState(true)
    const [user,setUser] = useState(null)
    const history = useHistory()

    useEffect(()=>{
        //firebase auth user function to return user data
        auth.onAuthStateChanged((user)=>{
            setUser(user)
            setLoading(false)
            if(user)history.push('/chats')
            console.log(user)
        })
    },[user,history])

    const value = {user}

    return(
        //useAuth pass the data from this all below to it child
        <AuthContext.Provider value={value}>
            {!loading&&children}
        </AuthContext.Provider>
    )
}