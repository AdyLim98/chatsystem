import React from 'react';
import { GoogleOutlined, FacebookOutlined } from '@ant-design/icons';

import firebase from "firebase/app";
import { auth } from '../firebase';

const Login = () =>{
    const googleSignIn = () =>{
        var response = auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider())
        console.log("Hi:",setTimeout(()=>response,10000))
    }
    return(
        //1)
        <div id="login-page">
            <div id="login-card">
                <h2>Welcome To My Chat System</h2>
                <div className="login-button google"
                    onClick={()=>googleSignIn()}
                >
                    <GoogleOutlined style={{paddingRight:10}}/>Sign In with Google
                </div>
                <div className="login-button facebook">
                    <FacebookOutlined style={{paddingRight:10}}
                        onClick={()=>auth.signInWithRedirect(new firebase.auth.FacebookAuthProvider())}
                    />Sign In with Facebook
                </div>
            </div>

        </div>
    )
}

export default Login;