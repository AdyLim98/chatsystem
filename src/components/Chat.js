import React,{useRef,useState,useEffect} from 'react';
// import firebase from 'firebase/app';
import { auth } from '../firebase';
import { useHistory } from 'react-router-dom';
import { ChatEngine } from 'react-chat-engine';
import env from "react-dotenv";

import { useAuth } from '../contexts/AuthContext';
import axios from 'axios';
//4
const Chat = () =>{
    const history = useHistory() ; 
    const {user} = useAuth()
    const [loading,setLoading] = useState(true) 
    console.log("USER:",user)
    
    useEffect(()=>{
        if(!user){
            history.push('/') 
            return
        }
        //existing user
        axios.get( 
            'https://api.chatengine.io/users/me/', 
            { headers: {
                "project-id": process.env.REACT_APP_CHATENGINE_PROJECTID,
                "user-name": user.email,
                "user-secret": user.uid
            }
        }).then((response)=>{
            setLoading(false)
            console.log("Sucess",response)
        }).catch((err)=>{
            console.log("ERR:",err)
            //if error with no user (so we create a new user)
            //formdata function is the default function provided by js (to create user)
            let formData = new FormData()
            formData.append('email',user.email)
            formData.append('username',user.email)
            formData.append('secret',user.uid)

            getImageFile(user.photoURL)
                .then((avatar)=>{
                    formData.append('avatar',avatar,avatar.name)

                    axios.post('https://api.chatengine.io/users/',formData,{
                        headers:{
                            "private-key":process.env.REACT_APP_ENGINE_KEY
                        }
                    }).then(()=>{
                        setLoading(false)
                    }).catch((err)=>{
                        console.log("ERR2:",err)
                    })
                })
        })
    },[user,history])

    const getImageFile = async(url)=>{
        const response = await fetch(url)
        //blob function used for convert image file or some others file into binary format
        const data = await response.blob()

        return new File([data],"userPhoto.jpg",{type:'image/jpeg'})
    }

    const logOut = () =>{
        auth.signOut();
        console.log("Here We Are",auth.signOut())
        alert("You had sign out!!!")
        history.push('/')
    }
    if(!user || loading) return 'Loading...'
    return(
        <div className="chats-page">
            <div className="nav-bar">
                <div className="logo-tab">
                    Chat System
                </div>
                <div className="logout-tab" onClick={logOut}>
                    Logout
                </div>
            </div>
            <ChatEngine 
                height="calc(100vh-66px)"
                projectID={process.env.REACT_APP_CHATENGINE_PROJECTID}
                userName={user.email}
                userSecret={user.uid}
            />
            
        </div>
    )
}

export default Chat;